using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LEVELSELECT : MonoBehaviour
{
    public void LEVEL1 ()
    {
        SceneManager.LoadScene("Level 1");
    }
    public void LEVEL2()
    {
        SceneManager.LoadScene("Level 2");
    }
    public void LEVEL3()
    {
        SceneManager.LoadScene("Level 3");
    }
    public void Ludacrismode()
    {
        SceneManager.LoadScene("Level 4");
    }
}
