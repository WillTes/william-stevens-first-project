using UnityEngine;

public class PlayerCollision : MonoBehaviour {

	public PlayerMovement movement;
	public GameObject fractured;
	public GameObject Score;
	void OnCollisionEnter (Collision collisionInfo)
	{
		
		if (collisionInfo.collider.tag == "Obstacle")
		{
			movement.enabled = false;
			
			Score.SetActive(false);
			
			FindObjectOfType<GameManager>().EndGame();
			
			Destroy(gameObject);
			
			Vector3 oldPos = transform.position;

			Instantiate(fractured, oldPos, Quaternion.identity);

			

		}
	}
}
