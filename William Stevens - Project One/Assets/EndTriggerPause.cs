using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTriggerPause : MonoBehaviour
{
	void OnTriggerEnter()
	{
		Time.timeScale = 0f;
	}
}
